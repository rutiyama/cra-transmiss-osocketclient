package br.com.softcenter.client.app;

import java.awt.SystemTray;
import java.awt.TrayIcon.MessageType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import com.localhost.systemtrayicon.TrayIconCRAClient;

public class ClientSocketConnection {
	public static void main(String args[]){
		try {
			//01. Cria um Socket para este cliente:
			Socket cliente = new Socket("127.0.0.1", 2015);

		   BufferedReader in =
			        new BufferedReader(
			            new InputStreamReader(cliente.getInputStream()));
		   
		   String userInput;
		   new TrayIconCRAClient();
		   while ((userInput = in.readLine()) != null) {
			   SystemTray.getSystemTray().getTrayIcons()[0].displayMessage("Alerta", userInput, MessageType.NONE);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
